
export class MyInputComponent extends HTMLElement {

    get inputEl() {
        if (! this._inputEl) {
            this._inputEl = this.el.querySelector('input');
        }
        return this._inputEl;
    }

    get el() {
        return this.shadowRoot;
    }

    onInput(event) {
        this.el.dispatchEvent(new CustomEvent('myinput', { bubbles: true, composed: true, detail: { value: event.target.value }}));
    }

    render() {
        this.attachShadow({ mode: 'open' });
        let span = document.createElement('span');
        span.insertAdjacentHTML('beforeend', `<input />`);
        this.el.appendChild(span);
    }

    bindEvents() {
        this.inputHandler = this.onInput.bind(this);
        this.inputEl.addEventListener('input', this.inputHandler);
    }

    connectedCallback() {
        this.render();
        this.bindEvents();
    }

    disconnectedCallback() {
        if (this.inputHandler) {
            this.inputEl.removeEventListener('input', this.inputHandler);
        }
    }
}